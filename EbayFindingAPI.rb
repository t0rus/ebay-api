# EbayFindingAPI
# --------------
#
# Author: Taylor Bockman <taylor.bockman@gmail.com>
#
#
# File that encapsulates the leg work of communicating with the Ebay API
# in Ruby. This implements a small subset of the full functionality of the
# eBay finding API. Mostly what I find useful for my own personal projects.
#
# For all of the functions that return ack, the user is required to parse
# success and failure themselves.
#
# If an error occurs this, the proper way to check is to first check if the
# function you called returns an ACK of "Failure", and then call error getter
# to get the hash of error details.
#
#
# TODO: * Full RDoc comments.
#       * Function that returns the amount of API calls remaining for the day
#       * If API supports it - Arbitrary item lookup for individual items
#         Support result sorting
#         then use the merge method of hashes.
#       * Unit tests


require 'json'
require 'open-uri'
require_relative 'Helpers'


module EbayAPI

	#base class for searches in this module
	class EbayProductSearch

		attr_accessor :appid, :userAgent
		attr_reader :globalid, :response, :page, :ack, :count, :error

		def initialize(appid, appType="production", globalid)
			@appid = appid

			@uri = if(appType.downcase == "sandbox")
				      URI.parse("http://svcs.sandbox.ebay.com/services" \
				      	        "/search/FindingService/v1")
			       else
			       	URI.parse("http://svcs.ebay.com/services" \
			       			      "/search/FindingService/v1")
			       end
			
			@page = 1
			@globalid = globalid.empty? ? "EBAY-US" : globalid
		end

		#Converts ack to boolean
		def ackSuccessful(ack)

			if(@ack == "Success")
				return true
			else
				return false
			end

		end

		#ruby inheirits private members unlike every other language in the world.
		private

			#Builds the GET request. additionalParams must contain the service
			#OPERATION-NAME and the operation specific inputs as a hash.
			def buildQueryParams(additionalParams)
				
				if(!additionalParams.empty?) 

					#Since GET request arguments can be out of order...
					params = {
						'SERVICE-VERSION' => "1.0.0",
						'SECURITY-APPNAME' => @appid,
						'RESPONSE-DATA-FORMAT' => "JSON",
						'GLOBAL-ID' => @globalid,
						'REST-PAYLOAD' => "",
						'paginationInput.pageNumber' => @page
					}.merge(additionalParams)


				else
					#This case cause it to fail in a safe way
					params = {
						'SERVICE-VERSION' => "1.0.0",
						'SECURITY-APPNAME' => @appid,
						'RESPONSE-DATA-FORMAT' => "JSON",
						'GLOBAL-ID' => @globalid,
						'REST-PAYLOAD' => "",
						'paginationInput.pageNumber' => @page
					}
				end

				return params
			end


	end

	#Class to simplify the configuration process of filters.
	#Each search function takes one of these as an optional argument
	#Corresponding values for the arguments can be found at 
	#http://developer.ebay.com/DevZone/finding/CallRef/types/ItemFilterType.html
	class ItemFilter
		
		attr_reader :filter

		def initialize
			@filter = {}
			@conditionFilter = {}
			@authFilter = {}
			@listingTypeFilter = {}
			@maxPriceFilter = {}
			@minPriceFilter = {}
			@fCount = 0
		end

		#Generates the condition itemfilter. Condition must be a valid ebay
		#item filter or the API call will return an error.
		#Expects an array of conditions with the first element being the main
		#condition.
		#subsequent calls clear the sub-hash and rebuild it with new values.
		def condition(conds)
			if(@conditionFilter.has_value?('Condition'))
				@conditionFilter = {}
			end

			if(!(conds.kind_of?(Array) || conds.kind_of?(String)))
				return false
			end

			@conditionFilter["itemFilter(#{@fCount}).name"] = 'Condition'

			i = 0
			conds.each do |cond|
				@conditionFilter["itemFilter(#{@fCount}).value(#{i})"] = cond
				i += 1
			end

			@fCount += 1

			return true
		end

		#Filter to only allow authorized sellers.
		#Authorized seller filters include true or false (include or dont)
		def authorizedSellerOnly(val)
			#Prevent double counting the filter count when recalled.
			if(@authFilter.has_value?('AuthorizedSellerOnly'))
				@authFilter = {}
			end

			if(!Helpers.isBoolean(val))
				return false
			end


			@authFilter["itemFilter(#{@fCount}).name"] = 'AuthorizedSellerOnly'
			@authFilter["itemFilter(#{@fCount}).value(0)"] = String(val)

			@fCount += 1

			return true
		end

		#Filter to only allow certain listing types. Examples would be
		#Auction, AuctionWithBIN, FixedPrice
		def listingType(types)
			if(@listingTypeFilter.has_value?('ListingType'))
				@listingTypeFilter = {}
			end

			if(!types.kind_of?(Array))
				return false
			end

			@listingTypeFilter["itemFilter(#{@fCount}).name"] = 'ListingType'
			i = 0
			types.each do |type|         
				@listingTypeFilter["itemFilter(#{@fCount}).value(#{i})"] = type
				i += 1
			end
			@fCount += 1

			return true
		end

		#Filter to set maximum price to a decimal value given by price
		#Let eBay return the error message if the price is in an invalid format
		#(decimal >= 0.0)
		def maxPrice(price)
			if(@maxPriceFilter.has_value?('MaxPrice'))
				@maxPriceFilter = {}
			end

			if(!(price.kind_of?(Fixnum) || price.kind_of?(Float)) || price < 0.00)
				return false
			end

			@maxPriceFilter["itemFilter(#{@fCount}).name"] = 'MaxPrice'
			@maxPriceFilter["itemFilter(#{@fCount}).value(0)"] = String(price)

			@fCount += 1

			return true
		end

		#Filter to set min price to a decimal value given by price
		#Let eBay return the error message if the price is in an invalid format
		#(decimal >= 0.0)
		def minPrice(price)

			if @minPriceFilter.has_value?('MinPrice')
				@minPriceFilter = {}
			end

			if(!(price.kind_of?(Fixnum) || price.kind_of?(Float)) || price < 0.00)
				return false
			end

			@minPriceFilter["itemFilter(#{@fCount}).name"] = 'MinPrice'
			@minPriceFilter["itemFilter(#{@fCount}).value(0)"] = String(price)

			@fCount += 1


			return true

		end


		def generateFilter

			@filter = @filter.merge(@conditionFilter)
			@filter = @filter.merge(@authFilter)
			@filter = @filter.merge(@listingTypeFilter)
			@filter = @filter.merge(@maxPriceFilter)
			@filter = @filter.merge(@minPriceFilter)

			return @filter
		end
	end



	# Represents a search request to the eBay finding api using keywords.
	class EbayFindingAPI < EbayProductSearch

		#sortOrder must be a valid sortOrder specified in 
		#http://developer.ebay.com/DevZone/finding/CallRef/
		#findItemsByKeywords.html#Request.sortOrder
		def searchByKeywords(keywords, page, sortOrder='',itemFilter=nil)
			#return results into response, page number into page
			#return ack

			#Set page to requested user page
			@page = page

			queryParams = {'OPERATION-NAME' => "findItemsByKeywords",
					           'keywords' => URI.escape(keywords)}

			#Sanity check the optional argument before merging
			if(itemFilter.kind_of?(ItemFilter))
				queryParams = queryParams.merge(itemFilter.generateFilter)
			end

			if(sortOrder != '')
				queryParams["sortOrder"] = sortOrder
			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)

			if @userAgent.empty?
				@userAgent = "EbaySearchByKeywords"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:findItemsByKeywordsResponse][0][:ack][0]

			if ackSuccessful(@ack)

				@searchResults = 
					@response[:findItemsByKeywordsResponse][0][:searchResult][0]

				@count = Integer(@searchResults[:@count])

				#Get what page the query is on.
				paginationOutput = 
					@response[:findItemsByKeywordsResponse][0][:paginationOutput][0]
				
				@page = Integer(paginationOutput[:pageNumber][0])
				@error = {}

			else

				#Split into two to fit in 80 char limit.
				@error = @response[:findItemsByKeywordsResponse][0][:errorMessage][0]
				@error = @error[:error][0]
			end

			return ackSuccessful(@ack)
		end

		#getItem searches the current page for the item requested
		#returns a hash representing the item if the item was found by its ID
		#otherwise it returns blank.
		#
		#Note - This function is hit and miss depending on the frequency of new
		#depending on the frequency of page item changes (new items added).          
		def getProduct(searchItem)

			items = getProducts()

			if(ackSuccessful(@ack)) 
				items.each do |item|
					if item[:itemId][0] == searchItem
						return item
					end
				end
			end

			#So it doesn't return items if the if fails above...
			return {}

		end


		#Query with a referenceID - To find use catalog.ebay.com. Find an item
		#and then use the numeric portion of the EPID number.
		#returns ack
		#sortOrder must be a valid sortOrder specified in 
		#http://developer.ebay.com/DevZone/finding/CallRef/
		#findItemsByKeywords.html#Request.sortOrder
		def searchByReference(productId, page, sortOrder='', itemFilter=nil)

			#set page to their requested page.
			@page = page

			queryParams = {'OPERATION-NAME' => 'findItemsByProduct',
					         'productId.@type' => 'ReferenceID', 
					         'productId' => productId}

			#Sanity check the optional argument before merging
			if(itemFilter.kind_of?(ItemFilter))
				queryParams = queryParams.merge(itemFilter.generateFilter)
			end

			if(sortOrder != '')
				queryParams["sortOrder"] = sortOrder
			end


			queryParams = buildQueryParams(queryParams)



			@uri.query = URI.encode_www_form(queryParams)

			if @userAgent.empty?
				@userAgent = "EbaySearchByProduct"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:findItemsByProductResponse][0][:ack][0]

			if ackSuccessful(@ack)
				@searchResults = 
					@response[:findItemsByProductResponse][0][:searchResult][0]

				@count = Integer(@searchResults[:@count])

				#Get what page the query is on.
				paginationOutput = 
					@response[:findItemsByProductResponse][0][:paginationOutput][0]
				
				@page = Integer(paginationOutput[:pageNumber][0])
				@error = {}

			else
				#Split into two to fit in 80 char limit.
				@error = @response[:findItemsByProductResponse][0][:errorMessage][0]
				@error = @error[:error][0]
			end

			return ackSuccessful(@ack) 
		end


		#Query with a UPC
		#returns ack
		#sortOrder must be a valid sortOrder specified in 
		#http://developer.ebay.com/DevZone/finding/CallRef/
		#findItemsByKeywords.html#Request.sortOrder
		def searchByUPC(upc, page, sortOrder='', itemFilter=nil)

			#set page to their requested page.
			@page = page

			queryParams = {'OPERATION-NAME' => 'findItemsByProduct',
					           'productId.@type' => 'UPC', 
					           'productId' => upc}

			#Sanity check the optional argument before merging
			if(itemFilter.kind_of?(ItemFilter))
				queryParams = queryParams.merge(itemFilter.generateFilter)
			end

			if(sortOrder != '')
				queryParams["sortOrder"] = sortOrder
			end

			queryParams = buildQueryParams(queryParams)

	
			@uri.query = URI.encode_www_form(queryParams)

			if @userAgent.empty?
				@userAgent = "EbaySearchByProduct"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:findItemsByProductResponse][0][:ack][0]

			if ackSuccessful(@ack)

				@searchResults = 
					@response[:findItemsByProductResponse][0][:searchResult][0]

				@count = Integer(@searchResults[:@count])

				#Get what page the query is on.
				paginationOutput = 
					@response[:findItemsByProductResponse][0][:paginationOutput][0]
				
				@page = Integer(paginationOutput[:pageNumber][0])
				@error = {}
			else
				#Split into two to fit in 80 char limit.
				@error = @response[:findItemsByProductResponse][0][:errorMessage][0]
				@error = @error[:error][0]
			end

			return ackSuccessful(@ack) 
		end

		#Returns the portion of the result hash that is an array of item hashes
		def getResults
			if(!@response.empty? && ackSuccessful(@ack))
				return @searchResults[:item]
			else
				return {}
			end
		end


	end

end