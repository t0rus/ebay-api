# EbayShoppingAPI
# --------------
#
# Author: Taylor Bockman <taylor.bockman@gmail.com>
#
#
# This is the chopped down bare bones version of the eBay shopping API. 

# Important notes:
#
# This API presently only supports entry of site ids by their corresponding
# number located at: 
# http://developer.ebay.com/DevZone/XML/docs/WebHelp/wwhelp/wwhimpl/common
# /html/wwhelp.htm?context=eBay_XML_API&file=FieldDifferences-Site_IDs.html
#
# EBayTime is implemented as a non-static class to save typing for the use
# case of auction sniping. This way you can simply intialize the time class
# and call EbayTime.geteBayTime() as many times as you need.
#
#
# IncludeSelector fails safe. If you provide invalid (non-boolean) input it
# will fail and leave the value whatever it was before (default: false).
#
#
#
# TODO:
#
#FindProducts
#GetShippingCosts
#
#http://developer.ebay.com/DevZone/Shopping/docs/Concepts/ShoppingAPIGuide.html
#http://developer.ebay.com/DevZone/shopping/docs/CallRef/index.html#CallIndex

require 'json'
require 'open-uri'
require_relative 'Helpers'



module EbayAPI


	class EbayShoppingBase
		attr_accessor :appid, :userAgent
		attr_reader :response, :ack, :error, :siteid

		def initialize(appid, appType="Production", siteid=0)
			@appid = appid
			
			@uri = if(appType.downcase == "sandbox")
				      URI.parse("http://open.api.sandbox.ebay.com/shopping")
			       else
			       	URI.parse("http://open.api.ebay.com/shopping")
			       end

			@siteid = siteid
			@ack = ""
			@response = ""
			@results = ""
			@userAgent = ""

		end

		#Converts ack to boolean
		def ackSuccessful(ack)

			if(@ack == "Success")
				return true
			else
				return false
			end

		end



		#ruby inheirits private members unlike every other language in the world.
		private

			#Builds the GET request. additionalParams must contain the service
			#OPERATION-NAME and the operation specific inputs as a hash.
			def buildQueryParams(additionalParams)
				
				if(!additionalParams.empty?) 

					params = {
						'version' => "849",
						'appid' => @appid,
						'responseencoding' => "JSON",
						'siteid' => @siteid
					}.merge(additionalParams)


				else
					#This case cause it to fail in a safe way
					params = {
						'version' => "849",
						'appid' => @appid,
						'responseencoding' => "JSON",
						'siteid' => @siteid
					}
				end

				return params
			end
	end


	#For use with getSingleItem and others than can return more granular data
	#based on IncludeSelector fields. The user is expected to know which fields
	#to apply to each function. eBay will throw up an error if you don't.
	#Details on how to use includeselector can be found at:
	#http://developer.ebay.com/DevZone/Shopping/docs
	#/CallRef/GetSingleItem.html#Request.IncludeSelector
	#and also:
	#http://developer.ebay.com/DevZone/Shopping/docs
	#/CallRef/GetUserProfile.html#GetUserProfile
	class IncludeSelector

		def initialize
			@details = false
			@description = false
			@textDescription = false
			@shippingCosts = false
			@itemSpecifics = false
			@variations = false
			@compatibility = false
			@feedbackDetails = false
			@feedbackHistory = false
		end



		#custom setters for the instance variables to do type checking for bool 
		def details=(value)
			if Helpers.isBoolean(value)
				@details = value
			end
		end

		def description=(value)
			if Helpers.isBoolean(value)
				@description = value
			end
		end

		def textDescription=(value)
			if Helpers.isBoolean(value)
				@textDescription = value
			end
		end

		def shippingCosts=(value)
			if Helpers.isBoolean(value)
				@shippingCosts = value
			end
		end

		def itemSpecifics=(value)
			if Helpers.isBoolean(value)
				@itemSpecifics=(value)
			end
		end

		def variations=(value)
			if Helpers.isBoolean(value)
				@variations = value
			end
		end

		def compatibility=(value)
			if Helpers.isBoolean(value)
				@compatibility = value
			end
		end

		def feedbackDetails=(value)
			if Helpers.isBoolean(value)
				@feedbackDetails = value
			end
		end

		def feedbackHistory=(value)
			if Helpers.isBoolean(value)
				@feedbackHistory = value
			end
		end		


		def generateSelector

			selector = []
			selectorString = ""

			#return CSV of fields marked true.
			if @details
				selector.push('Details')
			end

			if @description
				selector.push('Description')
			end

			if @textDescription
				selector.push('TextDescription')
			end

			if @shippingCosts
				selector.push('ShippingCosts')
			end

			if @itemSpecifics
				selector.push('ItemSpecifics')
			end

			if @variations
				selector.push('Variations')
			end

			if @compatibility
				selector.push('Compatibility')
			end

			if @feedbackDetails
				selector.push('FeedbackDetails')
			end

			if @feedbackHistory
				selector.push('FeedbackHistory')
			end

			selectorString = selector.join(",")

			return selectorString
		end

	end


	class EbayShoppingAPI < EbayShoppingBase

		#GMT time. Use Ruby's built in processing for ISO 8601 to handle the
		#output.
		def geteBayTime

			queryParams = {'callname' => 'GeteBayTime'}

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)

			if @userAgent.empty?
				@userAgent = "eBayTimeAPI"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if ackSuccessful(@ack)

				return @response[:Timestamp]

			else

				return ""

			end

		end

		#Retrieves comprehensive information about a single listing
		#use ItemSelector to specify more detail. If no ItemSelector is
		#provided eBay will use it's default set.
		def getSingleItem(ebayId, itemSelector=nil)

			queryParams = {'callname' => 'GetSingleItem',
				             'ItemID' => ebayId}

			if itemSelector.is_a?(IncludeSelector)

				#Tack on the include selector
				queryParams["IncludeSelector"] = itemSelector.generateSelector

			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayGetSingleItem"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)


		end


		#getResults
		#
		#Returns results based on the function call
		#
		#Most item calls - returns the :Item value results that have a hash
		#of hashes of items
		#
		#Users - returns the :User value that have a hash of the User queried
		#
		#Popular items - returns the :ItemArray array. This is a difference than
		#the other item return results because you also have to loop through the
		#index.
		#
		def getResults
			if(!@response.empty? && ackSuccessful(@ack))
				if @response.has_key?(:Item)	       #Most things that deal with items
					return @response[:Item]	           
				elsif @response.has_key?(:User)	     #User profiles
					return @response[:User]
				elsif @response.has_key?(:ItemArray) #FindPopularItems
					return @response[:ItemArray][:Item]
				elsif @response.has_key?(:PopularSearchResult)
					return @response[:PopularSearchResult]
				else
					return {}
				end
			else
				return {}
			end
		end

		#Expects a comma delimited list of values. Nearly the same code as
		#getSingleItem. Didn't make a special case in getSingleItem because it
		#seems smarter to leave each function call explicit.
		def getMultipleItem(ebayIds, itemSelector=nil)

			queryParams = {'callname' => 'GetMultipleItems',
				             'ItemID' => ebayIds}

			if itemSelector.is_a?(IncludeSelector)

				#Tack on the include selector
				queryParams["IncludeSelector"] = itemSelector.generateSelector

			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayGetSingleItem"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)


		end

		#Limited to 10 items. Ideal for monitoring your own items you have up
		#for bid or to programmatically monitor your watch/bid list.

		#Takes a comma delimited list of (0, 10] product Ids.

		def getItemStatus(items)
			
			queryParams = {'callname' => 'GetItemStatus',
				             'ItemID' => items}

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayGetSingleItem"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)


		end

		#Takes an eBay userID as an argument
		def getUserProfile(userId, includeSelector=nil)
			
			queryParams = {'callname' => 'GetUserProfile',
				             'UserID' => userId}

			if includeSelector.is_a?(IncludeSelector)

				#Tack on the include selector
				queryParams["IncludeSelector"] = includeSelector.generateSelector

			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayGetSingleItem"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)

		end

		#findPopularItems takes query keywords and optionally categoryIDs to
		#include and exclude and returns up to 20 of the most popular items
		#determined via watches and other eBay magic.
		#
		#Arguments are passed to the function via a hash since all arguments are
		#conditionally optional based on what else is supplied.
		#
		#Note: Either queryKeywords or categoryIDs must be specified.
		#      This assumes that the user will read the eBay documentation and
		#      provide the correct inputs.
		#      Also, categoryIDs and excludeIDs can contain multiple categories by
		#      simply delimiting them list of them with commas.
		#
		#      
		def findPopularItems(opt = {})
	
			queryParams = {'callname' => 'FindPopularItems'};

			opt[:QueryKeywords] ||= ''
			opt[:CategoryIDs] ||= ''
			opt[:excludeIDs] ||= ''



			if(opt[:QueryKeywords].kind_of?(String) && !opt[:QueryKeywords].empty?)

				queryParams['QueryKeywords'] = URI.escape(opt[:QueryKeywords])

			end

			if(opt[:CategoryIDs].kind_of?(String) && !opt[:CategoryIDs].empty?)

				queryParams['CategoryID'] = opt[:CategoryIDs] 

			end

			if(opt[:excludeIDs].kind_of?(String) && !opt[:excludeIDs].empty?)

				queryParams['CategoryIDExclude'] = opt[:excludeIDs]

			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayFindPopularItems"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)

		end

		#FindPopularSearches rertieves commonly used search phrases that users
		#type when looking for products. You can specify it via all available
		#optional parameters passed via hash
		#
		#All parameters are techically optional, though at least 1 must be
		#specified.
		#
		#Parameters:
		#:CategoryIDs - Restrict search to an eBay category or categories
		#              to specify multiple pass a comma delimited list of them
		#
		#
		#:IncludeChildCategories - True/False, also returns keywords on child
		#                          categories
		#
		#
		#:MaxKeywords - Integer value, specifies max returned keywords.
		#               min: 1, max: 100, default: 20
		#
		#
		#:MaxResultsPerPage - Integer, maximum number of results per page
		#                     Default: 20
		#
		#:QueryKeywords - Specify search terms and eBay will return 5 related
		#                 search terms and 5 alternate search terms
		#
		def findPopularSearches(opt={})

			queryParams = {'callname' => 'FindPopularSearches'};

			opt[:CategoryIDs] ||= ''
			opt[:IncludeChildCategories] ||= ''
			opt[:MaxKeywords] ||= 20
			opt[:MaxResultsPerPage] ||= 20
			opt[:QueryKeywords] ||= ''

			if(opt[:CategoryIDs].kind_of?(String) && !opt[:CategoryIDs].empty?)
				queryParams['CategoryID'] = opt[:CategoryIDs]
			end

			if(opt[:IncludeChildCategories].kind_of?(String) &&
				 !opt[:IncludeChildCategories].empty?)

				queryParams['IncludeChildCategories'] = opt[:IncludeChildCategories]

			end

			if(opt[:MaxKeywords].kind_of?(Integer) && opt[:MaxKeywords] > 0)

				queryParams['MaxKeywords'] = opt[:MaxKeywords].to_s

			end

			if(opt[:MaxResultsPerPage].kind_of?(Integer) && 
				 opt[:MaxResultsPerPage] > 0)

				queryParams['MaxResultsPerPage'] = opt[:MaxResultsPerPage].to_s

			end

			if(opt[:QueryKeywords].kind_of?(String) && !opt[:QueryKeywords].empty?)

				queryParams['QueryKeywords'] = URI.escape(opt[:QueryKeywords])

			end

			params = buildQueryParams(queryParams)

			@uri.query = URI.encode_www_form(params)


			if @userAgent.empty?
				@userAgent = "EbayFindPopularSearches"
			end

			results = @uri.open('User-Agent' => @userAgent).read

			@response = JSON.parse(results, {:symbolize_names => true})

			@ack = @response[:Ack]

			if !ackSuccessful(@ack)

				@error = @response[:Errors][0]
				return ackSuccessful(@ack)
			end


			@error = {}
			return ackSuccessful(@ack)


		end

	end #end class

end #end module