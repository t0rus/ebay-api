#Helpers.rb
#
# Author: Taylor Bockman <taylor.bockman@gmail.com>
#
# Contains generic non-eBay related code used to help clean up some
# commonly performed tasks.


module Helpers

	#Test if it is truly boolean or just truthy/falsy
	def self.isBoolean(value)

		if value.is_a?(TrueClass) || value.is_a?(FalseClass)
			return true
		else
			return false
		end

	end 


end
