Ruby eBay API
=============

This library provides access to key functions for accessing, and retrieving eBay data using the Finding, Shopping, and Trading APIs. As such, this library does not contain many functions and represents a stripped down version of the API that contains only core functionality.

#Usage Examples:


##EbayFindingAPI


	:::ruby
	require './EbayFindingAPI'


	appid = "YOUR_APP_ID_HERE" #sandbox or production
	
	itemFilter = EbayAPI::ItemFilter.new

	result = itemFilter.condition(['New'])
	itemFilter.maxPrice(200)



	productSearch = EbayAPI::EbayFindingAPI.new(appid, "sandbox", "EBAY-US")
	productSearch.userAgent = 'TestEBaySearchAPICalls'


	if(productSearch.searchByUPC('2000011658403', 1, itemFilter))

		puts "ACK: #{productSearch.ack}"
		puts "Count: #{productSearch.count}"
		puts "Page: #{productSearch.page}\n"

		puts productSearch.getResults

	else
		puts productSearch.error

	end


	if(productSearch.searchByKeywords("ipod", 1, itemFilter))
		items = productSearch.getResults

		puts "ACK: #{productSearch.ack}"
		puts "Result Count: #{productSearch.count}"
		puts "Page: #{productSearch.page}"
		puts "Results:\n\n"
		puts "Parsed Items: \n\n"

		if(!items.nil? && !items.empty?)
			items.each do |item|
				item.each do |k, v|
					print k
					print " => "
					print v[0]
					print "\n"
				end
			end
		end

		print "\n"

	else

		puts productSearch.error
	end


##EbayShoppingAPI

This showcases just a few features of the EbayShoppingAPI class. Refer to the
source code or in the future, the (to be written) RDocs for more information.

	:::Ruby

	require './EbayShoppingAPI'

	appid = "YOUR_APP_ID_HERE" #sandbox or production

	shopping = EbayAPI::EbayShoppingAPI.new(appid)

	#Return current time as of call
	time = shopping.geteBayTime
	puts time

	if(shopping.findPopularItems({:QueryKeywords => 'ipod', :excludeIDs => '73834'}))
		
		results = shopping.getResults
		puts "Results returned: #{results.size}"

		results.each do |result|
			result.each do |k, v|
				puts "#{k} => #{v}"
			end
		end

	else

		puts shopping.error

	end

	puts "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n"

	if(shopping.findPopularSearches({:QueryKeywords => 'Nikon', :CategoryIDs => '31388', :MaxKeywords => 100}))

		results = shopping.getResults
		puts "Results returned: #{results.size}"

		results.each do |result|
			result.each do |k, v|
				puts "#{k} => #{v}"
			end
		end

	else

		puts shopping.error

	end


##Some Things Worth Noting

* eBay production and sandbox calls will return two different itemIds.