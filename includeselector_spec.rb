#IncludeSelector test script


require './EbayShoppingAPI'


#test each one by setting true, then generating the selector and seeing
#if you get what you expect. Test for all values first. Make sure each
#function only returns true when boolean.

describe EbayAPI::IncludeSelector, "#Details" do

	it "Should only accept booleans" do
		includeSelector = EBayAPI::IncludeSelector.new
		includeSelector.details('asdf')
		includeSelector.details(1234)
		includeSelector.details(0)
		includeSelector.details(1)
		includeSelector.details([true])
		includeSelector.details({true => true})

		selector = includeSelector.generateSelector

		selector.should eq('')
	end
end