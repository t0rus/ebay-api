#ItemFilter test script


require './EbayFindingAPI'


#################################
#  Begin ItemFilter unit tests  #
#################################

#Todo - put custom messages on each 'should' so you can tell what actually
#       caused the failure.

describe EbayAPI::ItemFilter, "#condition" do
	it "Doesn't allow anything but arrays and strings" do
		itemFilter = EbayAPI::ItemFilter.new
		testObject = EbayAPI::ItemFilter.new
		result = itemFilter.condition(1234)
		result.should eq(false)
		result = itemFilter.condition(testObject)
		result.should eq(false)
	end
end

describe EbayAPI::ItemFilter, "#authorizedSellerOnly" do
	it "Doesn't allow anything but booleans" do
		itemFilter = EbayAPI::ItemFilter.new
		testObject = EbayAPI::ItemFilter.new
		result = itemFilter.authorizedSellerOnly('asdf')
		result.should eq(false)
		result = itemFilter.authorizedSellerOnly(1234)
		result.should eq(false)
		result = itemFilter.authorizedSellerOnly(['asdf'])
		result.should eq(false)
		result = itemFilter.authorizedSellerOnly({'test' => 'test'})
		result.should eq(false)
		result = itemFilter.authorizedSellerOnly(testObject)
		result.should eq(false)
	end
end

describe EbayAPI::ItemFilter, "#minPrice" do
	it "Doesn't allow anything but positive numbers" do
		itemFilter = EbayAPI::ItemFilter.new
		testObject = EbayAPI::ItemFilter.new
		result = itemFilter.minPrice(50)
		result.should eq(true)
		result = itemFilter.minPrice(20.55)
		result.should eq(true)
		result = itemFilter.minPrice(-30)
		result.should eq(false)
		result = itemFilter.minPrice('price')
		result.should eq(false)
		result = itemFilter.minPrice(['test', 'test'])
		result.should eq(false)
		result = itemFilter.minPrice({'test' => 'test'})
		result.should eq(false)
		result = itemFilter.minPrice(testObject)
		result.should eq(false)

	end
end

describe EbayAPI::ItemFilter, "#maxPrice" do
	it "Doesn't allow anything but positive numbers" do
		itemFilter = EbayAPI::ItemFilter.new
		testObject = EbayAPI::ItemFilter.new
		result = itemFilter.maxPrice(50)
		result.should eq(true)
		result = itemFilter.maxPrice(20.55)
		result.should eq(true)
		result = itemFilter.maxPrice(-30)
		result.should eq(false)
		result = itemFilter.maxPrice('price')
		result.should eq(false)
		result = itemFilter.maxPrice(['test','test'])
		result.should eq(false)
		result = itemFilter.maxPrice({'test' => 'test'})
		result.should eq(false)
		result = itemFilter.maxPrice(testObject)
		result.should eq(false)
	end
end

describe EbayAPI::ItemFilter, "#listingType" do
	it "Doesn't allow anything but arrays" do
		itemFilter = EbayAPI::ItemFilter.new
		testObject = EbayAPI::ItemFilter.new
		result = itemFilter.listingType(['test', 'test1'])
		result.should eq(true)
		result = itemFilter.listingType('test')
		result.should eq(false)
		result = itemFilter.listingType(1234)
		result.should eq(false)
		result = itemFilter.listingType(-1234)
		result.should eq(false)
		result = itemFilter.listingType({'test' => 'test'})
		result.should eq(false)
		result = itemFilter.listingType(testObject)
		result.should eq(false)


	end
end

###below here, tests to generate individual filter generation
###all together filter generation, and then adding multiples of the same.
###this may fail on the multiple conditions case...